package ru.young.mindfulbot;

import lombok.extern.log4j.Log4j2;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.AnswerPreCheckoutQuery;
import org.telegram.telegrambots.meta.api.methods.groupadministration.KickChatMember;
import org.telegram.telegrambots.meta.api.methods.groupadministration.UnbanChatMember;
import org.telegram.telegrambots.meta.api.methods.send.SendInvoice;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Location;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.payments.LabeledPrice;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboard;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import ru.young.mindfulbot.handlers.CommandHandler;
import ru.young.mindfulbot.handlers.InsightHandler;
import ru.young.mindfulbot.handlers.LocationHandler;
import ru.young.mindfulbot.handlers.SuccessfulPaymentHandler;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import static ru.young.mindfulbot.handlers.LocationHandler.DEFAULT_TIMEZONE;

@Log4j2
public class MindfulBot extends TelegramLongPollingBot {

    private static final Properties PROPS;
    private static final String BOT_TOKEN;
    private static final String BOT_USERNAME;
    private static final String INVITE_LINK = "https://t.me/joinchat/AAAAAEwg5kO2W4CDWP0v4w";
    private static final Long CHANNEL_ID = -1001277224515L;
    private static final String INSIGHT_BUTTON = "Инсайт";
    private static final String SEND_LOCATION_BUTTON = "Отправить мою геопозицию";
    private static final String SUBSCRIBE_LINK = "Подписаться на канал с инсайтами";
    private static final String LOCATION_RECEIVED_MESSAGE = "Спасибо, увидимся!";
    private static final String PAYMENT_TOKEN = System.getenv("PAYMENT_TOKEN");
    public static final String SELF_PROGRAM = "Сам";
    public static final String SELF_PLUS_OTHERS_POSTS_PROGRAM = "Сам и другие";

    static {
        PROPS = Main.getProps();
        BOT_TOKEN = PROPS.getProperty("bot.token");
        BOT_USERNAME = PROPS.getProperty("bot.username");
    }

    public void onUpdateReceived(Update update) {
        log.debug("Update: {}", update.toString());
        if (update.hasPreCheckoutQuery()) {
            log.debug("PreCheckoutQuery: {}", update.getPreCheckoutQuery().toString());
            answerPreCheckoutQuery(update);
            return;
        }
        final Message message = update.getMessage();
        final Integer userId = update.getMessage().getFrom().getId();
        final long chatId = update.getMessage().getChatId();

        if (update.getMessage().hasSuccessfulPayment()) {
            log.debug("Successful Payment: {}", update.getMessage().getSuccessfulPayment().toString());
            final String[] payload = update.getMessage().getSuccessfulPayment().getInvoicePayload().split(",");
            SuccessfulPaymentHandler.handleSuccessfulQuery(this, Integer.parseInt(payload[0]), Long.parseLong(payload[1]), DEFAULT_TIMEZONE, payload[2]);
            return;
        }

        if (update.getMessage().hasLocation()) {
            final Location location = message.getLocation();
            final Float latitude = location.getLatitude();
            final Float longitude = location.getLongitude();
            LocationHandler.setTimeZone(latitude, longitude, userId);
            sendMessage(chatId, LOCATION_RECEIVED_MESSAGE);
            return;
        }

        if (update.getMessage().isReply()) {
            sendMessage(CHANNEL_ID, update.getMessage().getText());
            return;
        }
        if (message.isCommand() || messageHasCaption(update) && message.getCaption().startsWith("/")) {
            CommandHandler.handle(this, update);
            return;
        }

        if (message.getText().contains(INSIGHT_BUTTON)) {
            InsightHandler.handle(this, update);
            return;
        }

        if (message.getText().equals(SELF_PROGRAM) || message.getText().equals(SELF_PLUS_OTHERS_POSTS_PROGRAM)) {
            sendInvoice(message.getText(), Math.toIntExact(chatId), userId);
            return;
        }
    }

    public synchronized ReplyKeyboardMarkup setButtons() {
        ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
        replyKeyboardMarkup.setSelective(true);
        replyKeyboardMarkup.setResizeKeyboard(true);

        List<KeyboardRow> keyboard = new ArrayList<>();

        KeyboardRow firstKeyboardRow = new KeyboardRow();
        firstKeyboardRow.add(new KeyboardButton(SEND_LOCATION_BUTTON).setRequestLocation(true));
        firstKeyboardRow.add(new KeyboardButton(INSIGHT_BUTTON));
        KeyboardRow secondKeyboardRow = new KeyboardRow();
        secondKeyboardRow.add(new KeyboardButton(SELF_PROGRAM));
        secondKeyboardRow.add(new KeyboardButton(SELF_PLUS_OTHERS_POSTS_PROGRAM));
        keyboard.add(firstKeyboardRow);
        keyboard.add(secondKeyboardRow);

        return replyKeyboardMarkup.setKeyboard(keyboard);
    }

    public synchronized ReplyKeyboardMarkup setButtonsForUnsubscribed() {
        ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
        replyKeyboardMarkup.setSelective(true);
        replyKeyboardMarkup.setResizeKeyboard(true);
        List<KeyboardRow> keyboard = new ArrayList<>();
        KeyboardRow keyboardRow = new KeyboardRow();
        keyboardRow.add(new KeyboardButton(SELF_PROGRAM));
        keyboardRow.add(new KeyboardButton(SELF_PLUS_OTHERS_POSTS_PROGRAM));
        keyboard.add(keyboardRow);
        return replyKeyboardMarkup.setKeyboard(keyboard);
    }

    public synchronized InlineKeyboardMarkup setJoinChannelButton() {
        final InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> inlineKeyboard = new ArrayList<>();
        final List<InlineKeyboardButton> inlineKeyboardButtons = new ArrayList<>();
        inlineKeyboard.add(inlineKeyboardButtons);
        final InlineKeyboardButton inlineKeyboardButton = new InlineKeyboardButton();
        inlineKeyboardButtons.add(inlineKeyboardButton);
        inlineKeyboardButton.setText(SUBSCRIBE_LINK);
        inlineKeyboardButton.setUrl(INVITE_LINK);
        inlineKeyboardMarkup.setKeyboard(inlineKeyboard);
        return inlineKeyboardMarkup;
    }

    public synchronized void sendMessage(Long chatId, String messageText, ReplyKeyboard keyboard) {
        SendMessage sendMessage = new SendMessage();
        sendMessage.enableMarkdown(true);
        sendMessage.setChatId(chatId);
        sendMessage.setText(messageText);
        sendMessage.setReplyMarkup(keyboard);
        try {
            execute(sendMessage);
        } catch (TelegramApiException e) {
            log.error("Cannot send a message! %n{}", e.toString());
        }
    }

    public synchronized void sendMessage(Long chatId, String messageText) {
        SendMessage sendMessage = new SendMessage();
        sendMessage.enableMarkdown(true);
        sendMessage.setChatId(chatId);
        sendMessage.setText(messageText);
        try {
            execute(sendMessage);
        } catch (TelegramApiException e) {
            log.error("Cannot send a message! %n{}", e.toString());
        }
    }

    private static boolean messageHasCaption(Update update) {
        return update.getMessage().getCaption() != null;
    }

    private SendInvoice buildSelfProgramInvoice(Integer userId, Integer chatId) {
        SendInvoice invoice = new SendInvoice();
        ArrayList<LabeledPrice> prices = new ArrayList<>();
        LabeledPrice price = new LabeledPrice();
        price.setLabel("RUB");
        price.setAmount(16900);
        prices.add(price);
        invoice.setChatId(chatId);
        invoice.setTitle("Подписка " + SELF_PROGRAM);
        invoice.setDescription("Доступ к ежедневным вопросам\n" +
            "Возможность писать инсайты в конце дня\n");
        invoice.setPayload(userId + "," + chatId + "," + SELF_PROGRAM);
        invoice.setProviderToken(PAYMENT_TOKEN);
        invoice.setStartParameter("test");
        invoice.setCurrency("RUB");
        invoice.setPrices(prices);
        return invoice;
    }

    private SendInvoice buildSelfPlusOtherPostsProgramInvoice(Integer userId, Integer chatId) {
        SendInvoice invoice = new SendInvoice();
        ArrayList<LabeledPrice> prices = new ArrayList<>();
        LabeledPrice price = new LabeledPrice();
        price.setLabel("RUB");
        price.setAmount(29900);
        prices.add(price);
        invoice.setChatId(chatId);
        invoice.setTitle("Подписка " + SELF_PLUS_OTHERS_POSTS_PROGRAM);
        invoice.setDescription("Доступ к ежедневным вопросам\n" +
            "Возможность писать инсайты в конце дня\n" +
            "Возможность ежедневно читать инсайты других людей\n");
        invoice.setPayload(userId + "," + chatId + "," + SELF_PLUS_OTHERS_POSTS_PROGRAM);
        invoice.setProviderToken(PAYMENT_TOKEN);
        invoice.setStartParameter("test");
        invoice.setCurrency("RUB");
        invoice.setPrices(prices);
        return invoice;
    }

    public void sendInvoice(String program, Integer userId, Integer chatId) {
        SendInvoice invoice = null;
        switch (program) {
            case SELF_PROGRAM:
                invoice = buildSelfProgramInvoice(userId, chatId);
                break;
            case SELF_PLUS_OTHERS_POSTS_PROGRAM:
                invoice = buildSelfPlusOtherPostsProgramInvoice(userId, chatId);
        }
        try {
            execute(invoice);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

    private void answerPreCheckoutQuery(Update update) {
        String id = update.getPreCheckoutQuery().getId();
        AnswerPreCheckoutQuery preCheckoutQuery = new AnswerPreCheckoutQuery();
        preCheckoutQuery.setPreCheckoutQueryId(id);
        preCheckoutQuery.setOk(true);
        try {
            execute(preCheckoutQuery);
            log.debug("Precheckout query answered: {}", id);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

    public void unbanChatMember(Integer userId) {
        final UnbanChatMember unbanChatMember = new UnbanChatMember();
        unbanChatMember.setChatId(CHANNEL_ID);
        unbanChatMember.setUserId(userId);
        try {
            execute(unbanChatMember);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

    public void kickChatMember(Integer userId) {
        final KickChatMember kickChatMember = new KickChatMember();
        kickChatMember.setChatId(CHANNEL_ID);
        kickChatMember.setUserId(userId);
        try {
            execute(kickChatMember);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

    public String getBotUsername() {
        return BOT_USERNAME;
    }

    public String getBotToken() {
        return BOT_TOKEN;
    }
}
