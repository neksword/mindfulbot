package ru.young.mindfulbot.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Subscription {

    private boolean subscribed;
    @Column(name = "subscription_end_date")
    private java.sql.Date subscriptionEndDate;
}
