package ru.young.mindfulbot.entity;


import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@Entity
@Table(name = "questions_test")
public class Question {

    @Id
    @GeneratedValue(generator = "number_generator")
    private Integer number;
    private String formulation;
    @Column(name = "q_offset")
    private Integer offset;

    public Question(String formulation) {
        this.formulation = formulation;
    }
}
