package ru.young.mindfulbot.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "promo_test")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Promocode {

    @Id
    private String code;
    private Boolean used;
    private String program;
}
