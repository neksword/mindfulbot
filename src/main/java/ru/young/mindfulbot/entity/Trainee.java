package ru.young.mindfulbot.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "trainees_test")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Trainee {

    @Id
    private Integer userId;
    private long chatId;
    private String timezone;
    private String program;
    private Boolean insight;
    private Integer progress;
    private Subscription subscription;
}
