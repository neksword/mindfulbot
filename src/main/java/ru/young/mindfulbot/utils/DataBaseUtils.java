package ru.young.mindfulbot.utils;

import lombok.extern.log4j.Log4j2;

import java.sql.Date;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.*;

import static java.time.temporal.ChronoUnit.DAYS;

@Log4j2
public class DataBaseUtils {

    private static final String TABLE_NAME = "trainees";
    private static final String QUESTIONS_TABLE_NAME = "questions";
    private static final String PROMO_TABLE = "promo";
    private static volatile int offset = 1;
    private static String QUESTION_UPDATE_QUERY = "INSERT INTO " + QUESTIONS_TABLE_NAME + " (number, question) VALUES (?, ?)";
    private static String question;

    static {
        setOffset();
        log.debug("Offset: {}", offset);
        setQuestion();
        log.debug("Question: {}", question);
    }

    private static Connection getConnection() throws SQLException {
        String dbUrl = System.getenv("JDBC_DATABASE_URL");
        Connection connection = DriverManager.getConnection(dbUrl);
        connection.setAutoCommit(true);
        return connection;
    }

    public static String getQuestion() {
        return question;
    }

    private static Map<String, java.sql.Date> calculateSubscriptionDates() {
        HashMap<String, Date> dates = new HashMap<>();
        java.sql.Date subscriptionStartDate = new Date(Instant.now().truncatedTo(DAYS).toEpochMilli());
        dates.put("subscriptionStartDate", subscriptionStartDate);
        java.sql.Date subscriptionEndDate = new Date(Instant.now().truncatedTo(DAYS).plus(30, DAYS).toEpochMilli());
        dates.put("subscriptionEndDate", subscriptionEndDate);
        return dates;
    }

    public static void save(Integer userId, long chatId, String timeZone, String program) {
        final Map<String, java.sql.Date> dates = calculateSubscriptionDates();
        try (final Connection connection = getConnection()) {
            try (Statement statement = connection.createStatement()) {
                statement.executeUpdate("INSERT INTO " + TABLE_NAME + " (userid, chatid, timezone, subscribed, program, insight, progress, subscription_start_date, subscription_end_date) VALUES (" + userId + ", " + chatId + ", '" + timeZone + "', " + true + ", '" + program + "', " + null + ", " + 0 + ", '" + dates.get("subscriptionStartDate") + "', '" + dates.get("subscriptionEndDate") + "');");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static boolean userExists(Integer userId) {
        try (Connection connection = getConnection()) {
            try (final Statement statement = connection.createStatement()) {
                try (ResultSet resultSet = statement.executeQuery("SELECT * FROM " + TABLE_NAME + " WHERE userid=" + userId + ";")) {
                    if (resultSet.next()) {
                        return true;
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static void activateSubscription(Integer userId, String program) {
        final Map<String, java.sql.Date> dates = calculateSubscriptionDates();
        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        try (final Connection connection = getConnection()) {
            try (Statement statement = connection.createStatement()) {
                try (ResultSet resultSet = statement.executeQuery("SELECT \"subscribed\", \"subscription_end_date\" FROM " + TABLE_NAME + " WHERE userid=" + userId + ";")) {
                    if (resultSet.next() && resultSet.getBoolean("subscribed")) {
                        final Date currentSubscriptionEndDate = resultSet.getDate("subscription_end_date");
                        log.debug(currentSubscriptionEndDate);
                        final String formattedCurrentSubscriptionEndDate = simpleDateFormat.format(currentSubscriptionEndDate);
                        java.sql.Date newSubscriptionEndDate = new Date(Instant.parse(formattedCurrentSubscriptionEndDate).plus(30, DAYS).toEpochMilli());
                        log.debug(newSubscriptionEndDate);
                        dates.replace("subscriptionStartDate", currentSubscriptionEndDate);
                        dates.replace("subscriptionEndDate", newSubscriptionEndDate);
                        log.debug(dates.get("subscriptionStartDate"));
                        log.debug(dates.get("subscriptionEndDate"));
                    }
                }
                statement.executeUpdate("UPDATE " + TABLE_NAME + " SET subscribed=true, subscription_start_date='" + dates.get("subscriptionStartDate") + "', subscription_end_date='" + dates.get("subscriptionEndDate") + "', program='" + program + "' WHERE userid=" + userId + ";");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void deactivateSubscription(Integer userId) {
        try (final Connection connection = getConnection()) {
            try (Statement statement = connection.createStatement()) {
                statement.executeUpdate("UPDATE " + TABLE_NAME + " SET subscribed=false WHERE userid=" + userId + ";");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

//    public static boolean isSubscribed(Integer userid) {
//        boolean subscribed = false;
//        try (final Connection connection = getConnection()) {
//            try (Statement statement = connection.createStatement()) {
//                try (ResultSet resultSet = statement.executeQuery("SELECT \"subscribed\" FROM " + TABLE_NAME + " WHERE userid=" + userid + ";")) {
//                    if (!resultSet.next()) {
//                        return false;
//                    } else subscribed = resultSet.getBoolean(1);
//                }
//            }
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//        return subscribed;
//    }

    public static List<String> getUserTimezones() {
        final ArrayList<String> userTimezones = new ArrayList<>();
        try (final Connection connection = getConnection()) {
            try (final Statement statement = connection.createStatement()) {
                try (ResultSet resultSet = statement.executeQuery("SELECT DISTINCT \"timezone\" FROM " + TABLE_NAME + ";")) {
                    while (resultSet.next()) {
                        userTimezones.add(resultSet.getString(1));
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return userTimezones;
    }

    public static List<Long> getChatIdsByTimezone(String timezone) {
        final ArrayList<Long> chatIds = new ArrayList<>();
        try (final Connection connection = getConnection()) {
            try (final Statement statement = connection.createStatement()) {
                try (ResultSet resultSet = statement.executeQuery("SELECT \"chatid\" FROM " + TABLE_NAME + " WHERE timezone='" + timezone + "' AND subscribed=true;")) {
                    while (resultSet.next()) {
                        chatIds.add(resultSet.getLong(1));
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return chatIds;
    }

    private static synchronized void setOffset() {
        try (final Connection connection = getConnection()) {
            try (final Statement statement = connection.createStatement()) {
                try (ResultSet resultSet = statement.executeQuery("SELECT \"offset\" FROM questions WHERE number=1;")) {
                    resultSet.next();
                    offset = resultSet.getInt(1);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static synchronized void setQuestion() {
//        final String query = "SELECT \"question\" FROM questions WHERE number=" + offset + ";";
        try (final Connection connection = getConnection()) {
            try (final Statement statement = connection.createStatement()) {
                try (final ResultSet resultSet = statement.executeQuery("SELECT \"question\" FROM questions WHERE number=" + offset + ";")) {
                    if (resultSet.next()) {
                        question = resultSet.getString(1);
                    } else {
                        resetOffset(statement);
                        try (final ResultSet set = statement.executeQuery("SELECT \"question\" FROM questions WHERE number=" + offset + ";")) {
                            set.next();
                            question = set.getString(1);
                        }
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static void resetOffset(Statement statement) throws SQLException {
        statement.executeUpdate("UPDATE " + QUESTIONS_TABLE_NAME + " SET \"offset\"=1 WHERE number=1;");
        offset = 1;
    }

    //
    public static List<Long> getExpiringSubscriptions() {
        final List<Long> expiringSubscriptions = new ArrayList<>();
        final long currentDateInMillis = Instant.now().truncatedTo(DAYS).toEpochMilli();
        final Date currentDate = new Date(currentDateInMillis);
        try (final Connection connection = getConnection()) {
            try (Statement statement = connection.createStatement()) {
                try (ResultSet resultSet = statement.executeQuery("SELECT \"chatid\" FROM " + TABLE_NAME + " WHERE subscription_end_date >= '" + currentDate + "' AND subscription_end_date - '" + currentDate + "' <= 3 AND subscription_end_date - '" + currentDate + "' > 0;")) {
                    if (!resultSet.next()) {
                        return Collections.emptyList();
                    } else {
                        do {
                            expiringSubscriptions.add(resultSet.getLong(1));
                        } while (resultSet.next());
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return expiringSubscriptions;
    }

    public static Map<Integer, String> getUsersWithExpiredSubscription() {
        final HashMap<Integer, String> usersWithExpiredSubscription = new HashMap<>();
        final long currentDateInMillis = Instant.now().truncatedTo(DAYS).toEpochMilli();
        final Date currentDate = new Date(currentDateInMillis);
        try (Connection connection = getConnection()) {
            try (Statement statement = connection.createStatement()) {
                try (ResultSet resultSet = statement.executeQuery("SELECT \"userid\", \"program\" FROM " + TABLE_NAME + " WHERE '" + currentDate + "' - subscription_end_date = 0;")) {
                    if (!resultSet.next()) {
                        return Collections.emptyMap();
                    } else {
                        do {
                            usersWithExpiredSubscription.put(resultSet.getInt(1), resultSet.getString(2));
                        } while (resultSet.next());
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return usersWithExpiredSubscription;
    }

    public static void updateUserTimezone(String timezone, Integer userId) {
        try (Connection connection = getConnection()) {
            try (Statement statement = connection.createStatement()) {
                statement.executeUpdate("UPDATE " + TABLE_NAME + " SET timezone='" + timezone + "' WHERE userid=" + userId + ";");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void incrementOffset() {
        offset++;
        try (Connection connection = getConnection()) {
            try (Statement statement = connection.createStatement()) {
                statement.executeUpdate("UPDATE " + QUESTIONS_TABLE_NAME + " SET \"offset\"=" + offset + " WHERE number=1;");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void insertQuestions(Map<Integer, String> questions) {
        try (final Connection connection = getConnection()) {
            try (final PreparedStatement insertQuestionStatement = connection.prepareStatement(QUESTION_UPDATE_QUERY)) {
                try (final Statement truncateStatement = connection.createStatement()) {
                    truncateStatement.executeUpdate("TRUNCATE " + QUESTIONS_TABLE_NAME);
                }
                final Set<Integer> questionNumbers = questions.keySet();
                for (Integer questionNumber : questionNumbers) {
                    insertQuestionStatement.setInt(1, questionNumber);
                    insertQuestionStatement.setString(2, questions.get(questionNumber));
                    insertQuestionStatement.executeUpdate();
                }
            }
            try (final Statement insertOffsetStatement = connection.createStatement()) {
                insertOffsetStatement.executeUpdate("UPDATE " + QUESTIONS_TABLE_NAME + " SET \"offset\"=" + offset + " WHERE number=1;");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void storePromoCode(String promoCode, String program) {
        try (Connection connection = getConnection()) {
            try (Statement statement = connection.createStatement()) {
                statement.executeUpdate("INSERT INTO " + PROMO_TABLE + " (code, used, program) VALUES ('" + promoCode + "', false, '" + program + "');");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void setPromoCodeUsed(String promoCode) {
        try (Connection connection = getConnection()) {
            try (Statement statement = connection.createStatement()) {
                statement.executeUpdate("UPDATE " + PROMO_TABLE + " SET used=true WHERE code='" + promoCode + "';");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static boolean promoCodeUsed(String promoCode) {
        boolean used = true;
        try (Connection connection = getConnection()) {
            try (Statement statement = connection.createStatement()) {
                try (ResultSet resultSet = statement.executeQuery("SELECT \"used\" FROM " + PROMO_TABLE + " WHERE code='" + promoCode + "';")) {
                    resultSet.next();
                    used = resultSet.getBoolean(1);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return used;
    }

    public static String getPromoProgram(String code) {
        String program = null;
        try (Connection connection = getConnection()) {
            try (Statement statement = connection.createStatement()) {
                try (ResultSet resultSet = statement.executeQuery("SELECT \"program\" FROM " + PROMO_TABLE + " WHERE code='" + code + "';")) {
                    resultSet.next();
                    program = resultSet.getString(1);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return program;
    }

//    public static String getProgram(Integer userId) {
//        try (Connection connection = getConnection()) {
//            try
//        }
//    }
}
