package ru.young.mindfulbot.dao;

import ru.young.mindfulbot.dao.impl.PostgresDAOFactory;

public interface DAOFactory {

    static DAOFactory getInstance() {
        return new PostgresDAOFactory();
    }

    TraineeDAO getTraineeDAO();

    PromocodeDAO getPromocodeDAO();

    QuestionDAO getQuestionDAO();
}
