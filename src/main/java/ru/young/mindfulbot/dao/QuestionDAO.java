package ru.young.mindfulbot.dao;

import ru.young.mindfulbot.entity.Question;

import java.util.List;

public interface QuestionDAO {

    void insertQuestion(Question question);

    void updateQuestion(Question question);

    default Question findQuestion(int id) {
        return findQuestion(id);
    }

    Question findQuestion(Integer id);

    void insertQuestions(List<String> questions);

}
