package ru.young.mindfulbot.dao;

import ru.young.mindfulbot.entity.Trainee;

import java.util.List;

public interface TraineeDAO {

    void updateTrainee(Trainee trainee);

    void insertTrainee(Trainee trainee);

    Trainee findTrainee(Integer userId);

    List<Trainee> findTrainees(List<Integer> ids);
}
