package ru.young.mindfulbot.dao;

import ru.young.mindfulbot.entity.Promocode;

public interface PromocodeDAO {

    void insertPromocode(Promocode promocode);

    void updatePromocode(Promocode promocode);

    Promocode findPromocode(Promocode promocode);
}
