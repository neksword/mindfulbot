package ru.young.mindfulbot.dao.impl;

import org.hibernate.Session;
import org.hibernate.Transaction;
import ru.young.mindfulbot.dao.QuestionDAO;
import ru.young.mindfulbot.entity.Question;

import java.util.List;

import static ru.young.mindfulbot.dao.impl.PostgresDAOFactory.getSession;

public class PostgresQuestionDAO implements QuestionDAO {

    @Override
    public void insertQuestion(Question question) {
        Transaction transaction = null;
        try (Session session = getSession()) {
            transaction = session.beginTransaction();
            session.save(question);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            throw e;
        }
    }

    @Override
    public void updateQuestion(Question question) {
        Transaction transaction = null;
        try (final Session session = getSession()) {
            transaction = session.beginTransaction();
            session.update(question);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            throw e;
        }
    }

    @Override
    public Question findQuestion(Integer id) {
        Question question;
        final Session session = getSession();
        question = session.get(Question.class, id);
        return question;
    }

    @Override
    public void insertQuestions(List<String> questions) {
        Transaction transaction = null;
        final int listSize = questions.size();
        try (final Session session = getSession()) {
            transaction = session.beginTransaction();
            final Integer batchSize = 2;
            for (int i = 0; i < listSize; i++) {
                if (i > 0 && i % batchSize == 0) {
                    session.flush();
                    session.clear();
                }
                final String formulation = questions.get(i);
                Question question = new Question();
                question.setFormulation(formulation);
                session.save(question);
            }
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            throw e;
        }
    }
}
