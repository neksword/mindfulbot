package ru.young.mindfulbot.dao.impl;

import lombok.extern.log4j.Log4j2;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import ru.young.mindfulbot.dao.DAOFactory;
import ru.young.mindfulbot.dao.PromocodeDAO;
import ru.young.mindfulbot.dao.QuestionDAO;
import ru.young.mindfulbot.dao.TraineeDAO;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Properties;

@Log4j2
public class PostgresDAOFactory implements DAOFactory {

    private static final String DATABASE_URL = "postgres://ytbfsonzpiwbnl:0b683133de6d90731d903a8373617c30137ccbe4164c72f354c65fd20c694f36@ec2-34-202-88-122.compute-1.amazonaws.com:5432/d9c592hb54m6uc?sslmode=require";
    private static final SessionFactory SESSION_FACTORY = buildSessionFactory();

    private static Properties getDatabaseConnectionSettings() {
        URI dbUri;
        try {
            dbUri = new URI(DATABASE_URL);
        } catch (URISyntaxException e) {
            log.error("Malformed DB connection URL: {}", DATABASE_URL, e);
            throw new RuntimeException("Malformed DB connection URL: " + DATABASE_URL, e);
        }
        final Properties connectionSettings = new Properties();
        final String username = dbUri.getUserInfo().split(":")[0];
        connectionSettings.put("hibernate.connection.username", username);
        final String password = dbUri.getUserInfo().split(":")[1];
        connectionSettings.put("hibernate.connection.password", password);
        String url = "jdbc:postgresql://" + dbUri.getHost() +  ":" + dbUri.getPort() + dbUri.getPath() + "?" + dbUri.getQuery();
        connectionSettings.put("hibernate.connection.url", url);
        return connectionSettings;
    }

    private static SessionFactory buildSessionFactory() {
        final Properties connectionSettings = getDatabaseConnectionSettings();
        final StandardServiceRegistry registry = new StandardServiceRegistryBuilder()
            .applySettings(connectionSettings)
            .configure()
            .build();
        return new MetadataSources(registry).buildMetadata().buildSessionFactory();
    }

    public static Session getSession() {
        return SESSION_FACTORY.openSession();
    }

    @Override
    public TraineeDAO getTraineeDAO() {
        return new PostgresTraineeDAO();
    }

    @Override
    public PromocodeDAO getPromocodeDAO() {
        return new PostgresPromocodeDAO();
    }

    @Override
    public QuestionDAO getQuestionDAO() {
        return new PostgresQuestionDAO();
    }
}
