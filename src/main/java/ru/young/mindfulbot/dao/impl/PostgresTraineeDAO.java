package ru.young.mindfulbot.dao.impl;

import org.hibernate.MultiIdentifierLoadAccess;
import org.hibernate.Session;
import org.hibernate.Transaction;
import ru.young.mindfulbot.dao.TraineeDAO;
import ru.young.mindfulbot.entity.Subscription_;
import ru.young.mindfulbot.entity.Trainee;
import ru.young.mindfulbot.entity.Trainee_;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.sql.Date;
import java.time.Instant;
import java.util.List;

import static java.time.temporal.ChronoUnit.DAYS;
import static ru.young.mindfulbot.dao.impl.PostgresDAOFactory.getSession;

public class PostgresTraineeDAO implements TraineeDAO {


    @Override
    public void insertTrainee(Trainee trainee) {
        Transaction transaction = null;
        try (Session session = getSession()) {
            transaction = session.beginTransaction();
            session.save(trainee);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            throw e;
        }
    }

    private static java.sql.Date calculateSubscriptionEndDate() {
        return new Date(Instant.now().truncatedTo(DAYS).plus(30, DAYS).toEpochMilli());
    }

    @Override
    public Trainee findTrainee(Integer userId) {
        final Session session = getSession();
        return session.get(Trainee.class, userId);
    }

    public List<Trainee> getTraineesWithExpiredSubscription() {
        final Session session = getSession();
            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<Trainee> criteria = builder.createQuery(Trainee.class);
            Root<Trainee> root = criteria.from(Trainee.class);
            criteria.select(root);
            criteria.where(builder.equal(builder.currentDate(),
                                         root.get(Trainee_.subscription)
                                             .get(Subscription_.subscriptionEndDate)
                                             .as(Date.class)));
            return session.createQuery(criteria).getResultList();
    }

    @Override
    public void updateTrainee(Trainee trainee) {
        Transaction transaction = null;
        try (final Session session = getSession()) {
            transaction = session.beginTransaction();
            session.update(trainee);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            throw e;
        }
    }

    @Override
    public List<Trainee> findTrainees(List<Integer> ids) {
        final Session session = getSession();
        final MultiIdentifierLoadAccess<Trainee> multiIdentifierLoadAccess = session.byMultipleIds(Trainee.class);
        return multiIdentifierLoadAccess.multiLoad(ids);
    }

    public List<Long> getChatIdsByTimezone(String timezone) {
        final Session session = getSession();
            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
            Root<Trainee> root = criteria.from(Trainee.class);
            criteria.select(root.get(Trainee_.chatId));
            criteria.where(builder.equal(root.get(Trainee_.timezone), timezone),
                           builder.equal(root.get(Trainee_.subscription)
                                             .get(Subscription_.subscribed),
                                         true));
            criteria.distinct(true);
            return session.createQuery(criteria).getResultList();
    }
}
