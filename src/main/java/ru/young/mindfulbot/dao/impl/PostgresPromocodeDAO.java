package ru.young.mindfulbot.dao.impl;

import org.hibernate.Session;
import org.hibernate.Transaction;
import ru.young.mindfulbot.dao.PromocodeDAO;
import ru.young.mindfulbot.entity.Promocode;

import static ru.young.mindfulbot.dao.impl.PostgresDAOFactory.getSession;

public class PostgresPromocodeDAO implements PromocodeDAO {

    @Override
    public void insertPromocode(Promocode promocode) {
        Transaction transaction = null;
        try (final Session session = getSession()) {
            transaction = session.beginTransaction();
            session.save(promocode);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
                throw e;
            }
        }
    }

    @Override
    public void updatePromocode(Promocode promocode) {
        Transaction transaction = null;
        try (final Session session = getSession()) {
            transaction = session.beginTransaction();
            session.update(promocode);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
                throw e;
            }
        }
    }

    @Override
    public Promocode findPromocode(Promocode promocode) {
        final String code = promocode.getCode();
        Transaction transaction = null;
        try (final Session session = getSession()) {
            transaction = session.beginTransaction();
            transaction.commit();
            return session.get(Promocode.class, code);
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
                throw e;
            }
        }
        return null;
    }
}
