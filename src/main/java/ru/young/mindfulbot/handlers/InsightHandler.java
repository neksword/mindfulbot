package ru.young.mindfulbot.handlers;

import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ForceReplyKeyboard;
import ru.young.mindfulbot.MindfulBot;

public class InsightHandler {

    private static final String INSIGHT_REPLY = "А теперь честно опиши свои мысли";

    private InsightHandler() {
        throw new IllegalStateException("Utility class");
    }

    public static void handle(MindfulBot bot, Update update) {
        final Long chatId = update.getMessage().getChatId();
        final ForceReplyKeyboard forceReplyKeyboard = new ForceReplyKeyboard().setSelective(true);
        bot.sendMessage(chatId, INSIGHT_REPLY, forceReplyKeyboard);
    }
}
