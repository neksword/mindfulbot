package ru.young.mindfulbot.handlers;

import io.restassured.response.Response;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.telegram.telegrambots.facilities.filedownloader.TelegramFileDownloader;
import org.telegram.telegrambots.meta.api.objects.Document;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import ru.young.mindfulbot.MindfulBot;
import ru.young.mindfulbot.Scheduler;
import ru.young.mindfulbot.dao.DAOFactory;
import ru.young.mindfulbot.dao.impl.PostgresDAOFactory;
import ru.young.mindfulbot.utils.DataBaseUtils;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedHashMap;

import static io.restassured.RestAssured.given;
import static org.telegram.telegrambots.meta.ApiConstants.BASE_URL;
import static ru.young.mindfulbot.MindfulBot.SELF_PLUS_OTHERS_POSTS_PROGRAM;
import static ru.young.mindfulbot.handlers.LocationHandler.DEFAULT_TIMEZONE;
import static ru.young.mindfulbot.handlers.SuccessfulPaymentHandler.*;
import static ru.young.mindfulbot.utils.DataBaseUtils.*;

@Log4j2
public class CommandHandler {

    private static final PostgresDAOFactory POSTGRES_DAO_FACTORY = (PostgresDAOFactory) DAOFactory.getInstance();

    public static void handle(MindfulBot bot, Update update) {
        String command = update.getMessage().getText();
        log.debug("Command: {}", command);
        if (command != null) {
            if (command.startsWith("/start")) {
                final Long chatId = update.getMessage().getChatId();
                UnsubscribedRequestHandler.handleUnsubscribedRequest(bot, chatId);
            } else if (command.startsWith("/admin")) {
                handleAdminCommand(update, bot);
            } else if (command.startsWith("/promo")) {
                handlePromoCommand(update, bot);
            }
        } else if (update.getMessage().getCaption() != null && update.getMessage().getCaption().startsWith("/new")) {
            handleNewCommand(update, bot);
        }
    }

    private static void handleNewCommand(Update update, MindfulBot bot) {
        final Document document = update.getMessage().getDocument();
        final String fileId = document.getFileId();
        final Response responseFile = given()
            .param("file_id", fileId).
                when().
                get(BASE_URL + bot.getBotToken() + "/getFile");
        org.telegram.telegrambots.meta.api.objects.File fileToDownLoad = responseFile
            .body()
            .jsonPath()
            .getObject("result", org.telegram.telegrambots.meta.api.objects.File.class);
        log.debug("Response body: {}", responseFile.toString());
        final TelegramFileDownloader downloader = new TelegramFileDownloader(bot::getBotToken);
        File file = null;
        try {
            file = downloader.downloadFile(fileToDownLoad,
                                           new File(System.getProperty("user.dir") + System.getProperty("line.separator") + document
                                               .getFileName()));
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
        final Integer userId = update.getMessage().getFrom().getId();
        final Long chatId = update.getMessage().getChat().getId();
        if (userId == 251235033 || userId == 214396502 || userId == 583999720) {
            LinkedHashMap<Integer, String> questions = new LinkedHashMap<>();
            try (XSSFWorkbook workbook = new XSSFWorkbook(file)) {
                XSSFSheet sheet = workbook.getSheetAt(0);
                Iterator<Row> rows = sheet.iterator();
                Row row;
                String cellValue;
                int rowNumber = 0;
                while (rows.hasNext()) {
                    row = rows.next();
                    for (Cell cell : row) {
                        CellType cellType = cell.getCellType();
                        if (cellType == CellType.STRING) {
                            cellValue = cell.getStringCellValue();
                            rowNumber++;
                            questions.put(rowNumber, cellValue);
                        }
                    }
                }
            } catch (IOException | InvalidFormatException e) {
                e.printStackTrace();
            }
            DataBaseUtils.insertQuestions(questions);
        } else {
            bot.sendMessage(chatId, "Вы не можете использовать эту команду!", bot.setButtons());
        }
    }

    private static void handleAdminCommand(Update update, MindfulBot bot) {
        final Integer userId = update.getMessage().getFrom().getId();
        final Long chatId = update.getMessage().getChat().getId();
        final String program = update.getMessage().getText().substring(7).toLowerCase();
        final String capitalizedProgram = StringUtils.capitalize(program);
        log.debug("Program: {}", capitalizedProgram);
        if (userId == 251235033 || userId == 214396502 || userId == 583999720) {
            String promoCode = RandomStringUtils.randomAlphabetic(10);
            DataBaseUtils.storePromoCode(promoCode, capitalizedProgram);
            bot.sendMessage(chatId, promoCode, bot.setButtons());
        }
    }

    private static void handlePromoCommand(Update update, MindfulBot bot) {
        final String promoCode = update.getMessage().getText().split(" ")[1];
        final Long chatId = update.getMessage().getChat().getId();
        final Integer userId = update.getMessage().getFrom().getId();
        if (DataBaseUtils.promoCodeUsed(promoCode)) {
            bot.sendMessage(chatId, "Промо-код уже использован или введен некорректно!", bot.setButtons());
        } else {
            setPromoCodeUsed(promoCode);
            final String promoProgram = getPromoProgram(promoCode);
            if (userExists(userId)) {
                activateSubscription(userId, promoProgram);
                bot.sendMessage(chatId, ACTIVATION_MESSAGE, bot.setButtons());
            } else {
                save(userId, chatId, DEFAULT_TIMEZONE, promoProgram);
                bot.sendMessage(chatId, WELCOME_MESSAGE, bot.setButtons());
            }
            if (promoProgram.equals(SELF_PLUS_OTHERS_POSTS_PROGRAM)) {
                bot.unbanChatMember(userId);
                log.debug("User unbanned");
                bot.sendMessage(chatId, CHANNEL_SUBSCRIPTION_MESSAGE, bot.setJoinChannelButton());
            }
            Scheduler.setUserDataChangeFlag(true);
        }
    }
}


