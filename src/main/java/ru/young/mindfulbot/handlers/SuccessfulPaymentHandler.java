package ru.young.mindfulbot.handlers;

import lombok.extern.log4j.Log4j2;
import ru.young.mindfulbot.MindfulBot;
import ru.young.mindfulbot.Scheduler;
import ru.young.mindfulbot.utils.DataBaseUtils;

import static ru.young.mindfulbot.MindfulBot.SELF_PLUS_OTHERS_POSTS_PROGRAM;
import static ru.young.mindfulbot.utils.DataBaseUtils.activateSubscription;
import static ru.young.mindfulbot.utils.DataBaseUtils.userExists;


@Log4j2
public class SuccessfulPaymentHandler {


    // TODO: Учесть, что при приобретении частичной подписки после полной, по истечении полной доступ в канал должен закрываться
    public static final String WELCOME_MESSAGE = "Оплата прошла успешно. Поздравляю, ты уже сделал первый маленький шаг к осознанности! \n" +
        "А теперь я расскажу как мы с тобой будем общаться:\n" +
        "С завтрашнего дня утром и днём я буду присылать тебе вопрос дня, а вечером спрошу что ты об этом думаешь. Ты можешь написать свои мысли и раньше, если чувствуешь, что готов - для этого нажми кнопку инсайт, а после напечатай сам текст. \n" +
        "Когда будешь писать инсайт, постарайся максимально полно описать почему ты так думаешь и как ты к этому относишься - это поможет тебе лучше узнать себя, а значит выйти на новый уровень!\n" +
        "Я всего лишь бот, и не могу с тобой поболтать, так что если у тебя возникают какие-то проблемы или вопросы, пожалуйста, пиши в @Bot_Young \n" +
        "Ну вот и всё, а для того, чтобы я мог отправлять тебе вопросы согласно твоему часовому поясу - отправь мне свою геопозицию.\n";
    public static final String ACTIVATION_MESSAGE = "Подписка активирована!";
    public static final String CHANNEL_SUBSCRIPTION_MESSAGE = "Нажми на кнопку ниже, чтобы подписаться на канал с инсайтами других подписчиков!";

    public static void handleSuccessfulQuery(MindfulBot bot, Integer userId, Long chatId, String timeZone, String program) {
        log.debug("Program: {}, userId: {}, chatId: {}", program, userId, chatId);
        if (userExists(userId)) {
            log.debug("User exists");
            activateSubscription(userId, program);
            bot.sendMessage(chatId, ACTIVATION_MESSAGE, bot.setButtons());
        } else {
            DataBaseUtils.save(userId, chatId, timeZone, program);
            bot.sendMessage(chatId, WELCOME_MESSAGE, bot.setButtons());
        }
        Scheduler.setUserDataChangeFlag(true);
        if (program.equals(SELF_PLUS_OTHERS_POSTS_PROGRAM)) {
            bot.unbanChatMember(userId);
            log.debug("User unbanned");
            bot.sendMessage(chatId, CHANNEL_SUBSCRIPTION_MESSAGE, bot.setJoinChannelButton());
        }
    }
}
