package ru.young.mindfulbot.handlers;

import lombok.extern.log4j.Log4j2;
import org.geonames.Timezone;
import org.geonames.WebService;
import ru.young.mindfulbot.Scheduler;
import ru.young.mindfulbot.utils.DataBaseUtils;

@Log4j2
public class LocationHandler {

    public static final String DEFAULT_TIMEZONE = "Europe/Moscow";

    private static String getTimeZone(float latitude, float longitude) {
        try {
            WebService.setUserName("neksword");
            final Timezone timezone = WebService.timezone(latitude, longitude);
            return timezone.getTimezoneId();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return DEFAULT_TIMEZONE;
    }

    public static void setTimeZone (float latitude, float longitude, Integer userId) {
        final String timeZone = getTimeZone(latitude, longitude);
        log.debug("Timezone: {}", timeZone);
        DataBaseUtils.updateUserTimezone(timeZone, userId);
        Scheduler.setUserDataChangeFlag(true);
    }
}
