package ru.young.mindfulbot;

import lombok.extern.log4j.Log4j2;
import ru.young.mindfulbot.utils.DataBaseUtils;

import java.util.*;
import java.util.concurrent.*;

import static ru.young.mindfulbot.MindfulBot.SELF_PLUS_OTHERS_POSTS_PROGRAM;
import static ru.young.mindfulbot.utils.DataBaseUtils.*;

@Log4j2
public class Scheduler {

    private static final ScheduledThreadPoolExecutor executor = (ScheduledThreadPoolExecutor) Executors.newScheduledThreadPool(5);
    private static final ExecutorService THREAD_POOL = Executors.newCachedThreadPool();
    private static final ArrayList<ScheduledFuture<?>> scheduledTasks = new ArrayList<>();
    private static final long DAY_IN_MILLIS = TimeUnit.DAYS.toMillis(1);
    private static final String INSIGHT_REMINDER = "Напиши свои инсайты по сегодняшней мысли:\n" +
        "Для того, чтобы прислать инсайт нажми кнопку, а после напиши сам текст инсайта";
    private static final String SUBSCRIPTION_EXPIRING_NOTIFICATION = "Подписка истекает!";
    private static final String SUBSCRIPTION_EXPIRED_NOTIFICATION = "Подписка истекла!";
    private static boolean userDataChangeFlag = false;
    private static String question;

    private enum MessageNumber {
        FIRST,
        SECOND,
        THIRD
    }

    public static void init(MindfulBot bot) {
        executor.setRemoveOnCancelPolicy(true);
        question = getQuestion();
        scheduleQuestionUpdate();
        scheduleTrainerTasks(bot);
        scheduleNewTraineeOrTimezoneUpdateCheck(bot);
        scheduleSubscriptionExpirationNotification(bot);
        scheduleDeactivatingAndKickingUnsubscribed(bot);
    }

    public static void setUserDataChangeFlag(boolean userDataChangeFlag) {
        Scheduler.userDataChangeFlag = userDataChangeFlag;
    }

    private static void scheduleTaskForTimezone(MindfulBot bot, String timezone, MessageNumber messageNumber, List<Long> chatIdsByTimezone) {
        final Calendar currentDay = Calendar.getInstance();
        final Calendar scheduledDay = Calendar.getInstance(TimeZone.getTimeZone(timezone));
        scheduledDay.set(Calendar.MINUTE, 0);
        scheduledDay.set(Calendar.SECOND, 0);
        scheduledDay.set(Calendar.MILLISECOND, 0);
        switch (messageNumber) {
            case FIRST:
                scheduledDay.set(Calendar.HOUR_OF_DAY, 8);
                break;
            case SECOND:
                scheduledDay.set(Calendar.HOUR_OF_DAY, 13);
                break;
            case THIRD:
                scheduledDay.set(Calendar.HOUR_OF_DAY, 21);
                break;
        }
        final long initialDelay;
        long currentTimeInMillis = currentDay.getTimeInMillis();
        long scheduledTimeInMillis = scheduledDay.getTimeInMillis();
        if (currentTimeInMillis < scheduledTimeInMillis) {
            initialDelay = scheduledTimeInMillis - currentTimeInMillis;
        } else {
            initialDelay = (scheduledTimeInMillis + DAY_IN_MILLIS) - currentTimeInMillis;
        }
        final ScheduledFuture<?> scheduledTask = executor.scheduleWithFixedDelay(() -> {
            for (Long chatId : chatIdsByTimezone) {
                if (messageNumber == MessageNumber.THIRD) {
                    THREAD_POOL.execute(() -> bot.sendMessage(chatId, INSIGHT_REMINDER, bot.setButtons()));
                    log.debug("Reminder sent");
                } else {
                    THREAD_POOL.execute(() -> bot.sendMessage(chatId, question, bot.setButtons()));
                    log.debug("Question sent");
                }
            }
        }, initialDelay, DAY_IN_MILLIS, TimeUnit.MILLISECONDS);
        scheduledTasks.add(scheduledTask);
    }

    private static void dropScheduledTasks() {
        for (ScheduledFuture<?> task : scheduledTasks) {
            task.cancel(false);
            log.debug("Task dropped");
        }
        scheduledTasks.clear();
    }

    private static void scheduleTrainerTasks(MindfulBot bot) {
        final List<String> userTimezones = DataBaseUtils.getUserTimezones();
        for (String timeZone : userTimezones) {
            log.debug("Task scheduled");
            final List<Long> chatIdsByTimezone = DataBaseUtils.getChatIdsByTimezone(timeZone);
            scheduleTaskForTimezone(bot, timeZone, MessageNumber.FIRST, chatIdsByTimezone);
            scheduleTaskForTimezone(bot, timeZone, MessageNumber.SECOND, chatIdsByTimezone);
            scheduleTaskForTimezone(bot, timeZone, MessageNumber.THIRD, chatIdsByTimezone);
        }
    }

    private static void onNewTraineeJoinedOrUserDataChanged(MindfulBot bot) {
        dropScheduledTasks();
        scheduleTrainerTasks(bot);
        setUserDataChangeFlag(false);
    }

    private static void scheduleNewTraineeOrTimezoneUpdateCheck(MindfulBot bot) {
        final long currentTimeMillis = System.currentTimeMillis();
        final Calendar executionTime = Calendar.getInstance();
        executionTime.set(Calendar.HOUR_OF_DAY, 23);
        executionTime.set(Calendar.MINUTE, 0);
        final long executionTimeInMillis = executionTime.getTimeInMillis();
        final long initialDelay = executionTimeInMillis - currentTimeMillis;
        executor.scheduleWithFixedDelay(() -> {
            if (userDataChangeFlag) {
                onNewTraineeJoinedOrUserDataChanged(bot);
                log.debug("User data changes detected, rescheduling....");
            }
        }, initialDelay, DAY_IN_MILLIS, TimeUnit.MILLISECONDS);
    }

    private static void scheduleSubscriptionExpirationNotification(MindfulBot bot) {
        executor.scheduleWithFixedDelay(() -> {
            final List<Long> expiringSubscriptions = getExpiringSubscriptions();
            if (!expiringSubscriptions.isEmpty()) {
                for (Long chatId : expiringSubscriptions) {
                    THREAD_POOL.execute(() -> bot.sendMessage(chatId, SUBSCRIPTION_EXPIRING_NOTIFICATION, bot.setButtons()));
                }
            }
        }, 0, DAY_IN_MILLIS, TimeUnit.MILLISECONDS);
    }

    private static void scheduleQuestionUpdate() {
        final Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 22);
        calendar.set(Calendar.MINUTE, 0);
        long initialDelay;
        long currentTimeInMillis = System.currentTimeMillis();
        long scheduledTimeInMillis = calendar.getTimeInMillis();
        if (currentTimeInMillis < scheduledTimeInMillis) {
            initialDelay = scheduledTimeInMillis - currentTimeInMillis;
        } else {
            initialDelay = (scheduledTimeInMillis + DAY_IN_MILLIS) - currentTimeInMillis;
        }
        executor.scheduleWithFixedDelay(() -> {
            incrementOffset();
            setQuestion();
            question = getQuestion();
            log.debug("Question updated: {}", question);
        }, initialDelay, DAY_IN_MILLIS, TimeUnit.MILLISECONDS);
    }

    public static void scheduleDeactivatingAndKickingUnsubscribed(MindfulBot bot) {
        final Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 22);
        calendar.set(Calendar.MINUTE, 0);
        long initialDelay;
        long currentTimeInMillis = System.currentTimeMillis();
        long scheduledTimeInMillis = calendar.getTimeInMillis();
        if (currentTimeInMillis < scheduledTimeInMillis) {
            initialDelay = scheduledTimeInMillis - currentTimeInMillis;
        } else {
            initialDelay = (scheduledTimeInMillis + DAY_IN_MILLIS) - currentTimeInMillis;
        }
        executor.scheduleWithFixedDelay(() -> {
            final Map<Integer, String> usersWithExpiredSubscription = getUsersWithExpiredSubscription();
            if (!usersWithExpiredSubscription.isEmpty()) {
                final Set<Integer> userIds = usersWithExpiredSubscription.keySet();
                for (Integer userId : userIds) {
                    deactivateSubscription(userId);
                    setUserDataChangeFlag(true);
                    log.debug("User deactivated: {}", userId);
                    bot.sendMessage((long) userId, SUBSCRIPTION_EXPIRED_NOTIFICATION);
                    log.debug("Notification sent");
                    if (usersWithExpiredSubscription.get(userId).equals(SELF_PLUS_OTHERS_POSTS_PROGRAM)) {
                        bot.kickChatMember(userId);
                        log.debug("User kicked: {}", userId);
                    }
                }
            }
        }, initialDelay, DAY_IN_MILLIS, TimeUnit.MILLISECONDS);
    }
}
