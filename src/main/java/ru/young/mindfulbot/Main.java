package ru.young.mindfulbot;

import lombok.extern.log4j.Log4j2;
import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

@Log4j2
public class Main {

    private static final Properties properties = new Properties();

    public static Properties getProps() {
        return properties;
    }

    public static void main(String[] args) {

        try {
            properties.load(new FileReader(new File("src/main/resources/config.properties")));
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.setProperty("user.timezone", "GMT");

        try {
            ApiContextInitializer.init();
            TelegramBotsApi telegramBotsApi = new TelegramBotsApi();
            final MindfulBot bot = new MindfulBot();
            telegramBotsApi.registerBot(bot);
            Scheduler.init(bot);
            System.out.println("Ready!");
            log.debug("Bot is registered.");
        } catch (TelegramApiException e) {
            log.fatal("The Bot could not be registered! Check proxy settings. Aborting. %n {}", e.toString());
        }
    }
}
